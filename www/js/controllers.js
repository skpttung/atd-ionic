angular.module('starter.controllers', ['ionic'])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
	$scope.chats = Chats.all();
	$scope.remove = function(chat) {
		Chats.remove(chat);
	}
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
	$scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
	$scope.settings = {
		enableFriends: true
	};
})

.controller('SMSCtrl', function($scope, $ionicPopup, $ionicLoading) {
	$scope.text = {
		sendto: '01262777110',
		textmsg: 'www.atadi.vn',
	};

	var toast = function (str) {
		$ionicLoading.show({ template: str, noBackdrop: true, duration: 2000 });
	}

	var checkDevice = function() {
		if ((/(ipad|iphone|ipod|android)/i.test(navigator.userAgent))) {
			toast('You are on mobile');
			return true;
		} else {
			toast('You are not on mobile');
			return false;
		}
	}

	$scope.isOnMobile = checkDevice();

	if (!$scope.isOnMobile) {
		toast('ALERT: Need run on mobile device for full functionalities')
	} else {
		document.addEventListener('deviceready', function () {
			if (!SMS) {
				toast('SMS plugin not ready');
				return;
			}

			document.addEventListener('onSMSArrive', function (e) {
				var data = e.data;
				smsList.push(data);

				toast('SMS_RECEIVED:', data);

			});
		}, false);
	}

	$scope.test = function () {
		var confirmPopup = $ionicPopup.confirm({
			title: $scope.text.sendto,
			template: $scope.text.textmsg
		});
		confirmPopup.then(function(res) {
			if(res) {
				console.log('You are sure');
			} else {
				console.log('You are not sure');
			}
		});
	}

	$scope.smsList = [];
	$scope.interceptEnabled = false;

	// $scope.update = function (id, str) {
	// 	$('div#' + id).html(str);
	// }

	// $scope.updateStatus = function (str) {
	// 	$('div#status').html(str);
	// }

	// $scope.updateData = function (str) {
	// 	$('div#data').html(str);
	// }

	$scope.sendSMS = function () {
		var sendto = $scope.text.sendto;
		var textmsg = $scope.text.textmsg;
		console.log(sendto, textmsg);

		if (sendto.indexOf(";") >= 0) {
			sendto = sendto.split(";");
			for (i in sendto) {
				sendto[i] = sendto[i].trim();
			}
		}

		var str = 'sendSMS' + sendto.toString() + textmsg.toString() + 'IsSMS' + SMS ? 'true' : 'false';
		toast(str);

		if (!SMS) toast('SMS PLUGIN NOT INSTALLED');
		SMS.sendSMS(sendto, textmsg, function() {}, function (err) {
			toast(err);
		});
	}

	$scope.listSMS = function () {
		if (!SMS) toast('SMS PLUGIN NOT INSTALLED'); 
		
		SMS.listSMS({}, function(data) {
			toast('sms listed as json array');

			var html = "";
			if (Array.isArray(data)) {
				for (var i in data) {
					var sms = data[i];
					html += sms.address + ": " + sms.body + ";";
				}
			}
			toast(html);

		}, function (err) {
			toast('error list sms: ' + err);
		});
	}
});
